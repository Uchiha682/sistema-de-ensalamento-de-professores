﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using Vuforia;

using System;

public class vbInteraction : MonoBehaviour, IVirtualButtonEventHandler
{
	VirtualButtonBehaviour[] virtualButtonBehaviours;
	string vbName;
	public GameObject btn1FirstPanel, btn2FirstPanel, btn1SecondPanel, btn2SecondPanel, perfil01, perfil02;

	void Start()
	{

		//Register with the virtual buttons TrackableBehaviour
		virtualButtonBehaviours = GetComponentsInChildren<VirtualButtonBehaviour>();

		for (int i = 0; i < virtualButtonBehaviours.Length; i++)
			virtualButtonBehaviours[i].RegisterEventHandler(this);
	}



	public void OnButtonPressed(VirtualButtonBehaviour vb)
	{
		vbName = vb.VirtualButtonName;

		if (vbName == "VirtualButton1") { 
			VirtualButton1 ();
			Deactivate ();
			Btn1 ();
		} else if (vbName == "VirtualButton2") {
			VirtualButton2 ();
			Deactivate ();
			Btn2 ();
		}
	}

	public void OnButtonReleased(VirtualButtonBehaviour vb)
	{

	}

	void VirtualButton1()
	{
		if (btn1FirstPanel.activeInHierarchy) {
			btn1FirstPanel.SetActive (false);
			btn2FirstPanel.SetActive (true);
		} 
	}

	void VirtualButton2()
	{
		if (btn2SecondPanel.activeInHierarchy) {
			btn1FirstPanel.SetActive (true);
			btn2FirstPanel.SetActive (false);
		}else{
			btn1SecondPanel.SetActive (false);
			btn2SecondPanel.SetActive (true);
		} 
	}

	void Deactivate()
	{
		perfil01.SetActive(false);
	}

	void Btn1()
	{
		perfil01.SetActive(true);
		perfil02.SetActive(false);
	}

	void Btn2()
	{
		perfil01.SetActive(false);
		perfil02.SetActive(true);
	}
		
}
